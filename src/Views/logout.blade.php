@extends('theme::layouts.basic')

@section('main-content')
<form action="{{ getLogoutUrl() }}" method="get" id="form-logout">
    {{--<input type="hidden" name="user_id" value="{{ auth()->user()->id }}">--}}
</form>
<script>
    document.getElementById('form-logout').submit()
</script>
@endsection