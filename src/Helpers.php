<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

function verifyUserLogin($request = [])
{
    $id = getUserAccess($request);

    $user = DB::table('v2_spk_employees')
                ->where('id', $id)
                ->first();

    if ($user) {
        session(['user_id', $user->id]);

        Auth::loginUsingId($user->id);

        return true;
    }

    return false;
}

function getLogoutUrl()
{
    $url = env('AUTH_SERVER_URL', 'https://siza.my');

    return $url;
}

function sizaScript()
{
    $js = file_get_contents(__DIR__.'/Stubs/siza.js.stub');

    return $js;
}