<?php

Route::any('/grant/access', function () {
    $input = request()->all();

    if (verifyUserLogin($input)) {
        return redirect()->route('home');
    }
    
    return redirect()->back();
})->middleware('web');

Route::get('/logout', function () {
    auth()->logout();

    return redirect()->to(getLogoutUrl())->send();
})->middleware('web');