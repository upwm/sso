<?php

namespace Ppzdev\Sso\Models;

use Spatie\Permission\Models\Role as RoleModel;

class Role extends RoleModel
{
    protected $guard_name = 'web';
}
