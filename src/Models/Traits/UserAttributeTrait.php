<?php

namespace Ppzdev\Sso\Models\Traits;

trait UserAttributeTrait
{
    /**
     * @return string
     */
    public function getAvatarAttribute()
    {
        if (file_exists(public_path('img/profile/'.$this->emp_id.'.png'))) {
            return asset( 'img/profile/' . $this->emp_id . '.png' );
        }

        return asset( 'img/profile/default.png' );
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function setNameAttribute()
    {
        $this->attributes['name'] = $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function getJantinaAttribute()
    {
        return $this->gender == '1' ? 'Lelaki' : 'Perempuan';
    }
}
