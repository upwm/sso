<?php

namespace Ppzdev\Sso\Models\Traits;

use SIZA\Auth\Models\Detail;

trait UserRelationTrait
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function detail()
    {
        return $this->hasOne(Detail::class, 'employee_id');
    }
}
