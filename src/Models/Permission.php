<?php

namespace Ppzdev\Sso\Models;

use Spatie\Permission\Models\Permission as PermissionModel;

class Permission extends PermissionModel
{
    public function childs()
    {
        return $this->hasMany(Permission::class, 'parent_id', 'id');
    }


}
