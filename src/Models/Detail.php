<?php

namespace Ppzdev\Sso\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'v2_spk_employee_details';

	protected $fillable = [
		'address',
		'street',
		'city',
		'postcode',
		'state',
		'address2',
		'street2',
		'city2',
		'postcode2',
		'state2',
		'date_of_birth',
		'phone_extension',
		'url',
		'phone_home',
		'phone_mobile',
		'detail_group',
		'lhdn',
		'lhdn_ordering',
		'pcb_category',
		'pcb_rebate',
		'pcb_individual_rebate',
		'zakat_percent',
		'kwsp',
		'kwsp_category',
		'kwsp_percent',
		'perkeso',
		'bank_id',
		'bank_account',
		'bank_branch',
		'counter_group_code',
		'marital_status_id',
		'gratuity',
		'oku',
		'salary_retired_month',
		'salary_retired_year',
		'first_contract_started_at',
		'first_contract_ended_at',
		'second_contract_started_at',
		'second_contract_ended_at',
		'tax_amount',
		'tax_zakat',
		'tax_payable',
	];

	protected $casts = [
		'address' => 'string',
		'street' => 'string',
		'city' => 'string',
		'postcode' => 'string',
		'state' => 'string',
		'address2' => 'string',
		'street2' => 'string',
		'city2' => 'string',
		'postcode2' => 'string',
		'state2' => 'string',
		'date_of_birth' => 'date',
		'phone_extension' => 'string',
		'url' => 'string',
		'phone_home' => 'string',
		'phone_mobile' => 'string',
		'detail_group' => 'string',
		'lhdn' => 'string',
		'lhdn_ordering' => 'string',
		'pcb_category' => 'string',
		'pcb_rebate' => 'integer',
		'pcb_individual_rebate' => 'integer',
		'zakat_percent' => 'float',
		'kwsp' => 'string',
		'kwsp_category' => 'integer',
		'kwsp_percent' => 'float',
		'perkeso' => 'string',
		'bank_id' => 'integer',
		'bank_account' => 'string',
		'bank_branch' => 'string',
		'counter_group_code' => 'string',
		'marital_status_id' => 'integer',
		'gratuity' => 'boolean',
		'oku' => 'boolean',
		'salary_retired_month' => 'string',
		'salary_retired_year' => 'integer',
		'first_contract_started_at' => 'date',
		'first_contract_ended_at' => 'date',
		'second_contract_started_at' => 'date',
		'second_contract_ended_at' => 'date',
		'tax_amount' => 'float',
		'tax_zakat' => 'float',
		'tax_payable' => 'float',
	];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(User::class);
    }
}
