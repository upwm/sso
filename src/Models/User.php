<?php

namespace Ppzdev\Sso\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ppzdev\Sso\Models\Traits\UserAttributeTrait;
use Ppzdev\Sso\Models\Traits\UserRelationTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, UserAttributeTrait, UserRelationTrait, HasRoles;

	/**
	 * The table name
	 *
	 * @var string
	 */
    protected $table = 'v2_siza_users';

    /**
     * The guard name for the model
     *
     * @var string
     */
	protected $guard_name = 'web';
}
