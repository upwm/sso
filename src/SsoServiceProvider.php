<?php

namespace Ppzdev\Sso;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class SsoServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/Routes/web.php');

        $this->loadViewsFrom(__DIR__.'/Views', 'sso');

        require_once __DIR__.'/Helpers.php';
    }
}